// test 1

function sayHey(name) {
	console.log('Hey ' + name);
}

sayHey('Anya');

// task 1

function placeAnOrder (orderNumber) {

	console.log ("Cust. order: ", orderNumber);

	cookAndDeliver(function(){
		console.log("Deliver food order: ", orderNumber);
	})
}

function cookAndDeliver (callback){
	setTimeout(callback, 5000);
}

placeAnOrder(1);
placeAnOrder(2);
placeAnOrder(3);
placeAnOrder(4);
placeAnOrder(5);


// task 2

var Bucky = {

	favFood: "bacon",
	favMovie: "Chappie"
};

var newPerson = Bucky;

newPerson.favFood = "salad";

console.log(Bucky.favFood);


//task 3

console.log(25 == "25"); //true
console.log(25 === "25"); //false


// task 4


var chap = {
	printName: function(){
		console.log("I am a chap");
		console.log(this===chap);
	}
};

chap.printName();


function monkey (){

	console.log("\nI am a monkey");
	console.log(this===global); // true bc function is not related to any object and is global
}

monkey();


//task 5

function user (){
	this.name = "";
	this.life = 100;
	this.giveLife = function giveLife(targetUser){
		targetUser.life += 1;
		this.life -= 1;
		console.log (this.name + " gave life to " + targetUser.name);
	}

}

var NameA = new user();
var NameB = new user();

NameA.name = "NameA";
NameB.name = "NameB";

NameA.giveLife(NameB);

console.log("NameA: " + NameA.life);
console.log("NameB: " + NameB.life);

user.prototype.uppercut = function uppercut(targetUser){
	targetUser.life -= 3;
	this.life += 3;

	console.log(this.name + " just uppercutted " + targetUser.name)
}

NameB.uppercut(NameA);

console.log("NameA: " + NameA.life);
console.log("NameB: " + NameB.life);


user.prototype.magic = 60;

console.log(NameA.magic);
console.log(NameB.magic);



// task 6

var mod = require('./mod1');

mod.printAvatar();


// task 7


console.log(favMovie);















